nnoremap <silent> + :call ShowCommandList({
      \ 'Lint': ExecFn(':T npm run lint')
      \ })<CR>

let g:neoformat_enabled_javascript = ['prettier', 'flow']
let g:jsx_ext_required = 0

nnoremap <silent> <leader>t :FlowType<CR>
nnoremap <silent> <leader>f :PrettierAsync<CR>
