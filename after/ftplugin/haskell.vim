let g:neomake_haskell_enabled_makers = ['ghcmod']

nnoremap <silent> <leader>t :FlowType<CR>
