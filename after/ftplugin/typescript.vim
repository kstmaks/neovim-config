nnoremap <silent> <leader>t :TSType<CR>
nnoremap <silent> <leader>d :TSDefPreview<CR>

function! s:ts_search_fzf()
  exec ':TSSearchFZF ' . input('Typescript search: ')
endfunction

nnoremap <silent> + :call ShowCommandList({
      \ 'Rename': ExecFn(':TSRename'),
      \ 'Go to definition': ExecFn(':TSDef'),
      \ 'Restart': ExecFn(':TSRestart'),
      \ 'Search': function('<SID>ts_search_fzf'),
      \ 'Run lint': function('neoterm#do', ['npm run lint']),
      \ })<CR>
