call plug#begin('~/.nvim/plugged')

" ========== Globals =====================

" Git
Plug 'airblade/vim-gitgutter' " signs
Plug 'tpope/vim-fugitive' " controls
Plug 'gregsexton/gitv' " tree viewer
Plug 'idanarye/vim-merginal' " manipulating branches

" Additional functionality
" Plug 'roxma/nvim-completion-manager'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'kassio/neoterm'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'rhysd/devdocs.vim'
Plug 'neomake/neomake'
Plug 'sbdchd/neoformat'
Plug 'scrooloose/nerdtree'
Plug 'Numkil/ag.nvim'
Plug 'haya14busa/incsearch.vim'
" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': 'bash install.sh',
"     \ }

" Appearance
Plug 'flazz/vim-colorschemes'
Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
"Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'fneu/breezy'
Plug 'josuegaleas/jay'
Plug 'nightsense/seabird'
Plug 'Badacadabra/vim-archery'
Plug 'nightsense/vimspectr'
Plug 'tomasiser/vim-code-dark'
" Plug 'vim-scripts/ScrollColors'

" " Behaviour
Plug 'ntpeters/vim-better-whitespace'
Plug 'editorconfig/editorconfig-vim'
Plug 'cohama/lexima.vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'christoomey/vim-sort-motion'
Plug 'SirVer/ultisnips'
Plug 'bkad/CamelCaseMotion'
Plug 'peterrincker/vim-argumentative'
"Plug 'alvan/vim-closetag'

" Misc
Plug 'johngrib/vim-game-code-break'

" ========== Language specific ===========

" Javascript
Plug 'mxw/vim-jsx'
Plug 'Galooshi/vim-import-js'
" Plug 'jason0x43/vim-js-indent'
Plug 'pangloss/vim-javascript'
Plug 'jelera/vim-javascript-syntax'
" Plug 'wokalski/autocomplete-flow'
" Plug 'flowtype/vim-flow'
Plug 'prettier/vim-prettier', {
  \ 'do': 'npm install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown'] }

" Html / Css
Plug 'othree/html5.vim'
Plug 'hail2u/vim-css3-syntax'

" Typescript
Plug 'mhartington/nvim-typescript'
Plug 'leafgarland/typescript-vim'

" Coffeescript
Plug 'kchmck/vim-coffee-script'

" Haskell
Plug 'eagletmt/neco-ghc'
Plug 'parsonsmatt/intero-neovim'

" Python
" Plug 'zchee/deoplete-jedi'

call plug#end()

" Project specific configuration

if filereadable('Session.vim')
  source Session.vim
endif
