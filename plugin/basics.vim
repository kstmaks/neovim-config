set tabstop=2
set shiftwidth=2
set expandtab

set noswapfile
set nocompatible
set clipboard=unnamed
set mouse=a
set termguicolors

set number
set incsearch
set nohlsearch
set nocursorcolumn
set nocursorline
set norelativenumber
set noshowcmd

syntax on

filetype on
filetype indent on
filetype plugin on
filetype plugin indent on

au FocusLost * :wa " autosave
au BufWritePre * :StripWhitespace
