" Write/Quit
nmap ;w :w<cr>
command! W execute "w"
command! Wqa execute "wqa"
command! Qa execute "qa"
command! Q execute "q"

" Tabs
nnoremap <silent> tk :tabnew<CR>
nnoremap <silent> tj :tabclose<CR>
nnoremap <silent> tl :tabn<CR>
nnoremap <silent> th :tabp<CR>

" Quickfix
nnoremap <M-n> :cnext<CR>
nnoremap <M-p> :cprevious<CR>
nnoremap <M-c> :cclose<CR>

" Movement
nnoremap <C-l> $
nnoremap <C-h> ^

" Terminal
tnoremap <ESC><ESC> <C-\><C-n>

" Editing
au BufNewFile * silent! :exe ': !mkdir -p ' . escape(fnamemodify(bufname('%'),':p:h'),'#% \\')
nnoremap \n :e %:p:h/
nnoremap \N :!mkdir %:p:h/
map § `
map ± ~
nmap § `
nmap ± ~
imap § `
imap ± ~
lmap § `
lmap ± ~
omap § `
omap ± ~
nnoremap <M-e> :e
nmap <S-s>' csi'`
nnoremap <M-x> :noh<CR>

nnoremap <C-S-R> :exec 'undo' undotree()['seq_last']<CR>


" Additional
nnoremap <M-s> :source %<CR>
