" Files + devicons
function! s:fzf_dev(path)
  if a:path == ''
    let s:command = $FZF_DEFAULT_COMMAND
  else
    let s:command = $FZF_DEFAULT_COMMAND . ' ' . shellescape(a:path)
  endif

  function! s:edit_file(item)
    let parts = split(a:item, ' ')
    let file_path = join(parts[1:], ' ')
    execute 'silent e' file_path
  endfunction

  call fzf#run({
        \ 'source': s:command,
        \ 'sink': function('s:edit_file'),
        \ 'options': "+m -x --ansi --preview '~/.config/nvim/iconify/iconify_cat.sh {}'"
        \ })
endfunction

function! ExecFn(str)
  function! s:inner(str)
    silent! exec a:str
  endfunction

  return function('s:inner', [a:str])
endfunction

function! ShowCommandList(options)
  let values = keys(a:options)

  function! s:run_function(options, item)
    let Callback = get(a:options, a:item)
    call Callback()
  endfunction

  call fzf#run({
        \ 'source': values,
        \ 'sink': function('s:run_function', [a:options]),
        \ 'options': '-m -x +s',
        \ 'down': '20%'
        \ })
endfunction

let $FZF_DEFAULT_COMMAND = '~/.config/nvim/iconify/iconify.py ag -g ""'

nnoremap - :call <SID>fzf_dev('')<CR>
nnoremap <TAB> :call <SID>fzf_dev(expand('%:p:h'))<CR>
