let g:NERDTreeMinimalUI = 1
let g:NERDTreeQuitOnOpen = 1
let g:NERDTreeWinSize = 50
nnoremap <silent> _ :NERDTreeToggle<CR>

augroup nerdtree-mappings
  au!
  au FileType nerdtree nnoremap <silent> <C-C> :NERDTreeClose<CR>
augroup END
