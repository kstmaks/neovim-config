function! s:run_command()
  exec ':T ' . input('Command: ', '', 'shellcmd')
  exec ':Topen'
endfunction

function! s:train()
  exec ':T sl'
  exec ':Topen'
endfunction

function! s:cowsay()
  exec ':T cowsay Стэн пидор'
  exec ':Topen'
endfunction

function! TerminalRun(command)
  exec '1windo topleft100vsplit'
  exec 'terminal ' . a:command
  set wfw
  tnoremap <silent><buffer> <C-c> <C-\><C-n>:q!<CR>
endfunction

let g:neoterm_position = 'horizontal'
let g:neoterm_size = 10
let g:neoterm_fixedsize = 1
let g:neoterm_autoscroll = 1

nnoremap <silent> ,, :TREPLSendLine<CR>
nnoremap <silent> <M-TAB> :Ttoggle<CR>
nnoremap <silent> <M-r> :call <SID>run_command()<CR>
vnoremap <silent> , :'<,'>TREPLSendSelection<CR>

nnoremap <silent> <M-t> :call <SID>train()<CR>
nnoremap <silent> <M-C-t> :call <SID>cowsay()<CR>
