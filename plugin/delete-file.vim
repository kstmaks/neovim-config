function! s:delete_file()
  let answer = input('Delete file "' . expand('%') . '"? [y/N]: ')
  if answer == 'y'
    silent exec '!rm %'
    exec ':bdelete!'
  endif
endfunction

nnoremap <silent> <leader>d :call <SID>delete_file()<CR>
