set updatetime=250

nnoremap <silent> <C-g>s :Gstatus<CR>
nnoremap <silent> <C-g>w :Gwrite<CR>
nnoremap <silent> <C-g>P :Gpull<CR>
nnoremap <silent> <C-g>p :Gpush<CR>
nnoremap <silent> <C-g>c :Gcommit<CR>
nnoremap <silent> <C-g>v :Gitv<CR>
nnoremap <silent> <C-g>m :Merginal<CR>
