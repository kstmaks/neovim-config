" function! s:start_spinner()
"   let b:show_spinner = 1
" endfunction

" function! s:stop_spinner()
"   let b:show_spinner = 0
" endfunction

" function! GetNeomakeProgress()
"   return ''
" endfunction

" Enable automake on write
call neomake#configure#automake('w')
let g:neomake_javascript_eslint_exe = $PWD . '/node_modules/.bin/eslint'

" " Display progress
" call airline#parts#define_function('neomake_progress', 'GetNeomakeProgress')
" call airline#parts#define_condition('neomake_progress', "get(b:, 'show_spinner', 0) == 1")

" let g:airline_section_y = airline#section#create_right(['ffenc', 'neomake_progress'])

" augroup neomake_hooks
"     au!
"     autocmd User NeomakeJobInit :call <sid>start_spinner()
"     autocmd User NeomakeFinished :call <sid>stop_spinner()
" augroup END
