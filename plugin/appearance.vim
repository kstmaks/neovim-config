let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''

let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = ''
let g:airline#extensions#tabline#right_sep = ''
let g:airline#extensions#tabline#right_alt_sep = ''

set bg=dark

colorscheme codeschool
hi MatchParen guifg=black guibg=black

let g:lightline = {
      \ 'colorscheme': 'jellybeans',
      \ }

set fillchars+=vert:\ " keep space
" hi MatchParen guifg=white guibg=black
