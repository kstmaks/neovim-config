augroup plugins-devdocs
  autocmd!
  autocmd FileType javascript,jsx,typescript,css,scss nmap <buffer>K <Plug>(devdocs-under-cursor)
augroup END
