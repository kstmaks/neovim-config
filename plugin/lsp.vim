" " Required for operations modifying multiple buffers like rename.
" set hidden

" let s:path = fnamemodify(resolve(expand('<sfile>:p:h')), ':h') .
"       \ '/support/javascript-typescript-langserver/lib/language-server-stdio.js'

" let g:LanguageClient_serverCommands = {
"     \ 'javascript': ['node', s:path],
"     \ 'javascript.jsx': ['node', s:path],
"     \ }

" nnoremap <silent> L :call LanguageClient_textDocument_hover()<CR>
" nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
" nnoremap <silent> dg :bprevious<CR>
" nnoremap <silent> <M-l> :call ShowCommandList({
"       \ 'Rename': ExecFn(':call LanguageClient_textDocument_rename()')
"       \ })<CR>
