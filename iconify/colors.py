def make_color(back, fore):
    return {
            'back': '\x1b[48;5;%sm' % back,
            'fore': '\x1b[38;5;%sm' % fore,
            }

def color_str(text, color):
    if color is None:
        return text
    return '%s%s%s\x1b[0m' % (color['back'], color['fore'], text)

def color_str_fg(text, color):
    if color is None:
        return text
    return '%s%s\x1b[0m' % (color['fore'], text)

def color_str_bg(text, color):
    if color is None:
        return text
    return '%s%s\x1b[0m' % (color['back'], text)


colors = {
        "js": make_color("94", "220"),
        "ts": make_color("24", "111"),
        "css": make_color("89", "213"),
        "scss": make_color("89", "213"),
        "html": make_color("22", "113"),
        "sh": make_color("236", "252"),
        "json": make_color("88", "209"),
        "py": make_color("22", "40")
        }
