#!/bin/bash

ARGS="$@"
FILE_PATH=$(echo $ARGS | cut -d ' ' -f 2-)


[[ $(file --mime $FILE_PATH) =~ binary ]] &&
  echo $FILE_PATH is a binary file ||
  (highlight -O ansi -l $FILE_PATH ||
  coderay $FILE_PATH ||
  rougify $FILE_PATH ||
  ccat $FILE_PATH ||
  cat $FILE_PATH) 2> /dev/null | head -500
