#!/usr/bin/env python3

import sys
import os
import subprocess
from icons import icons
from colors import make_color, colors, color_str, color_str_fg
from ftypes import get_type

def get_ext(path):
    _, ext = os.path.splitext(path)
    ext = ext[1:] if ext is not None else ''
    return ext

def format_icon(icon):
    return ' %s ' % icon

def icon_for_path(path, ext):
    if os.path.isdir(path):
        return icons['dir']
    else:
        return icons[ext] if ext in icons else '-'

def icon_str(path):
    ext = get_ext(path)
    icon = icon_for_path(path, ext)
    color = colors[ext] if ext in colors else None
    return color_str(format_icon(icon), color)

def format_output(path):
    ftype = get_type(path)
    icon = icon_str(path)

    terms = path.split('/')
    colored_path = [
            color_str_fg(term, make_color(0, 240)) if i < len(terms) - 1 else term
            for i, term in enumerate(terms)
            ]
    colored_path = color_str_fg('/', make_color(0, 245)).join(colored_path)

    return '%s %s' % (icon, colored_path)

if __name__ == '__main__':
    command = sys.argv[1:]
    with subprocess.Popen(command, stdout=subprocess.PIPE) as pipe:
        while True:
            pipe.stdout.flush()
            path = pipe.stdout.readline().decode('utf8').replace('\n', '')
            if path != '':
                print(format_output(path))
                sys.stdout.flush()
            else:
                break
