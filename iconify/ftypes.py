import os
import re
from colors import make_color, color_str

image_re = re.compile(r'(svg|png|jpg|jpeg|gif)$', re.IGNORECASE)
view_re = re.compile(r'(html|ejs)$', re.IGNORECASE)
spec_re = re.compile(r'spec\.(js|ts)$', re.IGNORECASE)
style_re = re.compile(r'css$', re.IGNORECASE)
selectors_re = re.compile(r'\/selectors\/', re.IGNORECASE)
reducers_re = re.compile(r'\/reducers\/', re.IGNORECASE)
helpers_re = re.compile(r'\/helpers\/', re.IGNORECASE)
actions_re = re.compile(r'\/actions\/', re.IGNORECASE)
components_re = re.compile(r'\/components\/', re.IGNORECASE)
ang_component_re = re.compile(r'component\.ts$', re.IGNORECASE)

def get_type(path):
    if os.path.isdir(path):
        return color_str('dir', make_color(235, 241))

    if image_re.search(path):
        return color_str('image', make_color(235, 36))

    if spec_re.search(path):
        return color_str('spec', make_color(235, 50))

    if view_re.search(path):
        return color_str('view', make_color(235, 32))

    if style_re.search(path):
        return color_str('style', make_color(235, 91))

    if selectors_re.search(path):
        return color_str('select', make_color(235, 94))

    if reducers_re.search(path):
        return color_str('reducer', make_color(235, 90))

    if helpers_re.search(path):
        return color_str('helper', make_color(235, 72))

    if actions_re.search(path):
        return color_str('action', make_color(235, 165))

    if components_re.search(path) or ang_component_re.search(path):
        return color_str('comp', make_color(235, 99))

    return color_str('file', make_color(235, 241))
